# OIS 2015/2016

## Kratek uvod v Node.js

### Enostavni spletni strežnik

Hello World; testiranje in ponovni zagon; razvoj lokalno vs. Cloud9; spremenljivke na strežniku.

### Napredni spletni strežnik

Strežnik statičnih spletnih strani; uporaba dodatnih knjižnic; obvladovanje zahtev po virih, ki ne obstajajo; predpomnjenje; dinamične strani; pridobivanje podatkov uporabnika; shranjevanje v datoteko.